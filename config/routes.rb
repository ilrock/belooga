Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/backoffice', as: 'rails_admin'
  devise_for :users,
              :controllers => {
                :omniauth_callbacks => 'omniauth_callbacks',
                :registrations => "users/registrations",
                invitations: "users/invitations"
              }

  devise_scope :user do
    get 'users/sign_out', :to => "devise/sessions#destroy"
  end


  root to: 'pages#home'
  get '/about', to: "pages#about"
  get '/terms-and-conditions', to: "pages#terms_and_conditions"
  get '/thank-you-for-the-sign-up', :to => "pages#sign_up_thank_you"
  get '/prelaunch_thank_you', :to => "pages#prelaunch_thank_you"
  get '/robots.:format' => 'pages#robots'
  get '/coming-soon' => 'pages#coming_soon'
  get '/apartments/search', :to => 'apartments#search', :defaults => { :format => 'js' }
  post '/apartments/:id/thank', :to => 'apartments#thank', :defaults => {:format => 'json'}

  resources :amenities
  resources :apartments
  resources :coworking_spaces
  resources :feedbacks
  resources :cities

  # namespace :admin do
  # 	get 'dashboard', :to => 'pages#dashboard'
  #   resources :amenities
  # 	resources :apartments
  # 	resources :coworking_spaces
  # end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
