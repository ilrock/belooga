require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Belooga
  class Application < Rails::Application

  	config.serve_static_assets = true

  	config.paperclip_defaults = {
	    storage: :s3,
	    s3_region: ENV["AWS_REGION"],
	    s3_credentials: {
	      s3_host_name: ENV["AWS_HOST_NAME"],
	      bucket: ENV["AWS_BUCKET"],
	      access_key_id: ENV["ACCESS_KEY_ID"],
	      secret_access_key: ENV["SECRET_ACCESS_KEY"]
	    }
	  }
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
