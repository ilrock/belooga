class UserMailer < ApplicationMailer
	default from: "belooga.info@gmail.com"

	def welcome_mail(user)
	  @user = user
	  mail(to: @user.email, subject: 'Welcome to Belooga! Ready to change the world?')
	end
end
