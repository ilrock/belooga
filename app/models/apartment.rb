include Geokit::Geocoders
class Apartment < ApplicationRecord
  has_many :associations, :dependent => :destroy
  has_many :amenities, through: :associations
  has_many :contacts
  has_many :pictures, :dependent => :destroy
  belongs_to :user
  belongs_to :wifi_speed
  belongs_to :city


  #after_create :assign_geometry
  extend FriendlyId
  friendly_id :name, use: :slugged

  acts_as_mappable :default_units => :kms,
                   :lat_column_name => :lat,
                   :lng_column_name => :lng

  #validates :name, :street, :city, :country, presence: true

  scope :by_amenities, ->(ids_ary) { joins(associations: :amenity).where("amenities.id" => ids_ary) }
  scope :by_city, -> (city_id) { where city_id: city_id }
  scope :by_wifi_speed, ->(wifi_speed) {joins(:wifi_speed).where('wifi_speeds.speed >= ?', wifi_speed)}
  scope :by_min_stay, ->(min_stay) {where min_stay: min_stay}
  scope :by_bedrooms, ->(bedrooms) {where "bedrooms >= ?", bedrooms}
  scope :by_type, ->(apartment_type) {where apartment_type: apartment_type}

  def assign_geometry
    begin
      lat = Apartment.geocode(self.address).lat.to_f
      lng = Apartment.geocode(self.address).lng.to_f
    rescue
      lat = Apartment.geocode(self.city.name).lat.to_f
      lng = Apartment.geocode(self.city.name).lng.to_f
    ensure
      self.lat = lat
      self.lng = lng
    end
  end
end
