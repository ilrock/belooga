class Amenity < ApplicationRecord
  has_many :associations
  has_many :apartments, through: :associations

  validates :name, presence: true

  def merge_name_icon
  	return "#{self.name} <i class='#{self.icon}' aria-hidden='true'></i>".html_safe
  end
end
