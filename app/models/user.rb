require 'net/http'
require 'uri'
require 'json'
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :invitable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

   validates :name, :email, presence: true

   has_one :picture, :dependent => :destroy

   has_many :apartments

   before_create :subscribe_to_mailerlite, :send_welcome_email

   def self.get_all_thank_yous(user)
    thank_yous = 0
    user.apartments.map{|a| thank_yous+=a.thank_yous}
    return thank_yous
  end

   def self.find_for_facebook(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => access_token.email).first
      if registered_user
        return registered_user
      else
        user = User.create(
          provider: access_token.provider,
          uid: access_token.uid,
          email: data.email,
          password: Devise.friendly_token[0,20],
          name: data.first_name,
        )
      end
    end
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:provider => access_token.provider, :uid => access_token.uid).first

    if user
      return user
    else
      registered_user = User.where(:email => access_token.email).first
      if registered_user
        return registered_user
      else
        user = User.create(
          provider: access_token.provider,
          email: data["email"],
          uid: access_token.uid,
          password: Devise.friendly_token[0,20],
          name: data["first_name"]
        )
      end
    end
  end

  def subscribe_to_mailerlite
    api = Ptilinopus::API.new("a8eb3930ea7d577be1ca6b809f110814")
    if self.name
      api.call(:post, "subscribers/6114247", {email: self.email, name: self.name })
    else
      api.call(:post, "subscribers/6114247", {email: self.email})
    end
  end

  def send_welcome_email
    unless self.invitation_token
      from = Email.new(email: "info@belooga.co")
      to = Email.new(email: self.email)
      subject = "Welcome to Belooga"
      email_text = "Welcome to Belooga, #{self.name} <br>"
      email_text+= "It's very good to see you on board. We're really looking forward to seeing you contribute to the community with your knowloedge.<br>"
      email_text+= "Feel free to <a href='http://belooga.co/apartments/new'> upload new apartments </a> directly in the platform and help other people finding good places to stay.<br>"
      email_text+= "Here at Belooga, we've seen a problem with the whole accommodation thing and we thought that with the help of the community we could solve it.<br>"
      email_text+= "Anyways, enough talking about us, in order to build a community, We would love to know more about you. You can hit reply on this email and we will personally read it and reply.<br>"
      email_text+= "Thank you!"
      content = Content.new(type: "text/html", value: email_text)
      mail = SendGrid::Mail.new(from, subject, to, content)
      mail.template_id = '82ff248c-3492-481e-b8dc-6658c78db90d'
      sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"])
      response = sg.client.mail._("send").post(request_body: mail.to_json)
      puts response.status_code
      puts response.body
      puts response.headers
      end
  end
end
