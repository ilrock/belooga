class Contact < ApplicationRecord
  belongs_to :apartment
  before_create :assign_icon, :assign_html


  def assign_icon
  	case self.contact_type
  	  when "Website" then self.icon = "fa fa-link"
  	  when "Email" then self.icon = "fa fa-envelope"
  	  when "Facebook Page" then self.icon = "fa fa-facebook-square"
  	  when "Phone Number" then self.icon = "fa fa-phone"
    end
  end

  def assign_html
  	case self.contact_type
  	  when "Website" then self.html = "<p class='website'> <a class='show-link' href='#{self.value}'> #{self.value} </a></p>"
  	  when "Email" then self.html = "<p> <a class='show-link' href='mailto:#{self.value}'> #{self.value} </a></p>"
  	  when "Phone Number" then self.html = "<p> <a class='show-link' href='tel:#{self.value}'> #{self.value} </a></p>"
  	  when "Facebook Page" then self.html = "<p class='website'> <a class='show-link' href='#{self.value}'> #{self.value} </a></p>"
    end
  end
end
