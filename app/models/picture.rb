class Picture < ApplicationRecord
  belongs_to :apartment
  belongs_to :user

  has_attached_file :image,
    :path => ":rails_root/public/images/:id/:filename",
    :url  => "/images/:id/:filename",
    :default_url => "//s3.eu-west-2.amazonaws.com/belooga/Images/placeholder.png",
    styles: { medium: "1200x>" }

  do_not_validate_attachment_file_type :image



end
