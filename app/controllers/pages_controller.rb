class PagesController < ApplicationController
	def home
	  @apartments = Apartment.where(:approved => true)
	  @cities = @apartments.map {|apt| apt.city.name.downcase}.uniq!

	  if current_user
	  	cookies[:has_account] = true
	  end
	end

	def coming_soon
	  @disable_partials = true;
	end

	def sign_up_thank_you

	end

	def prelaunch_thank_you

	end

	def about

	end

	def terms_and_conditions

	end

	def robots
	  respond_to :text
	  expires_in 6.hours, public: true
	end
end
