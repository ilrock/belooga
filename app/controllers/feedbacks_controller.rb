class FeedbacksController < ApplicationController
	def new
		@feedback = Feedback.new
	end

	def create
		@feedback = Feedback.create(feedback_params)
		respond_to do |format|
			if @feedback.save
				from = Email.new(email: @feedback.email)
			    to = Email.new(email: "andrea.rocca3@gmail.com")
			    sender_name = @feedback.name || ""
			    subject = "[Belooga Feedback][#{@feedback.feedback_type}] New message from #{sender_name}"
			    content = Content.new(type: "text/html", value: @feedback.body)
			    mail = SendGrid::Mail.new(from, subject, to, content)
			    mail.template_id = '82ff248c-3492-481e-b8dc-6658c78db90d'
			    sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"])
			    response = sg.client.mail._("send").post(request_body: mail.to_json)
			    puts response.status_code
			    puts response.body
			    puts response.headers
			end
			format.html { redirect_to root_path, notice: 'Your feedback was submitted! You will hear from us very soon' }
		end
	end


	private
	  def feedback_params
	    params.require(:feedback).permit(:name, :email, :body, :feedback_type)
	  end
end
