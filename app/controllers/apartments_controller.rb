require "sendgrid-ruby"
include SendGrid
class ApartmentsController < ApplicationController
	before_filter :authenticate_user!, only: :new
	skip_before_action :verify_authenticity_token, only: :search
	before_action :is_the_owner, only: [:edit, :update]
	before_action :is_prelaunch, except: [:new, :create]
	def index
		if params[:lat] && params[:lng] && params[:city]
			if params[:lat] == "18.7060641" && params[:lng] == "98.98171630000002"
				lat = 18.8024267
				lng = 98.96718629999998
			else
				lat = params[:lat]
				lng = params[:lng]
			end
			@apartments = Apartment.within(10, :origin => [lat, lng]).all
			@city = params[:city]
		elsif params[:city]
			if params[:city] == "Chiang Mai"
				city =  "MAYA Lifestyle Shopping Center Chang Phueak Chiang Mai Thailand"
			else
				city = params[:city]
			end
			@city = params[:city]
			@apartments = Apartment.within(10, :origin => city)
		else
			@apartments = Apartment.where(:approved => true)
		end
		@cities = Apartment.where(:approved => true).map { |apartment| apartment.city.name }.uniq!
	end

	def new
		@apartment = Apartment.new
	end

	def thank
		@apartment = Apartment.friendly.find(params[:id])
		if cookies[:apt_thanked]
		  if !cookies[:apt_thanked].include?("apt-#{@apartment.id}")
		    @apartment.update_attributes(:thank_yous => @apartment.thank_yous + 1)
		    cookies[:apt_thanked]+="apt-#{@apartment.id}"
		    status = "Success"
		  else
		  	status = "Error"
		  end
		else
			@apartment.update_attributes(:thank_yous => @apartment.thank_yous + 1)
		    cookies[:apt_thanked]="apt-#{@apartment.id}"
		    status = "Success"
		end

		if @apartment.save
			respond_to do |format|
		      format.js { }
		      format.json {render json: {
		        :response => {
		        	:status => status,
		        	:thank_yous => @apartment.thank_yous
		        }
		      }}
			end
		end
	end

	def create
		@apartment = Apartment.create(apartment_params)
		if current_user
			@apartment.user = current_user
		else
			cookies[:invitation_email] = params[:invitation_email]
			@apartment.user = User.invite!(:email => params[:invitation_email]) do |u|
			  u.skip_invitation = true
			end
		end
		if params[:apartment][:home_owner] && params[:apartment][:home_owner] == "Yes"
			@apartment.home_owner = true
		else
			false
		end
		
		puts "///////////////////////"
		puts params[:apartment][:city]

		city = City.find_by_id params[:apartment][:city]

		if city
			@apartment.city = city
		else
			@apartment.city = City.create(name: params[:apartment][:city])
		end


		@apartment.description = params[:apartment][:description] if params[:apartment][:description]
		@apartment.wifi_speed = WifiSpeed.find_by_speed(params[:apartment][:wifi_speed])
		phone_number = params[:contact_phone]
		@apartment.contacts.push(Contact.create(:contact_type => "Phone Number", :value => phone_number))
		number_of_contacts = params[:contact_types].length
		number_of_contacts.times do |x|
		  @apartment.contacts.push(Contact.create(:contact_type => params[:contact_types][x], :value => params[:contact_values][x]))
		end
      	amenities = params[:apartment][:amenities]
		amenities.shift
		amenities.each do |amenity|
	  		@apartment.amenities.push Amenity.find(amenity)
		end

      	if params[:images]
	        first = true
	        params[:images].each { |image|
	          if first
	            @apartment.pictures.create(image: image, apartment: @apartment, front: true)
	            first = false
	          else
	          	@apartment.pictures.create(image: image, apartment: @apartment, front: false)
	          end
	        }
	    elsif params[:direct_uris]
	    	first = true
	        params[:direct_uris].each { |image|
	          if first
	            @apartment.pictures.create(direct_uri: image, apartment: @apartment, front: true)
	            first = false
	          else
	          	@apartment.pictures.create(direct_uri: image, apartment: @apartment, front: false)
	          end
	        }
	    end
		respond_to do |format|
	      	if @apartment.save
	      		send_user_email
	      		if current_user
				  format.html { redirect_to apartments_path, notice: 'apartment saved' }
				else
				  format.html { redirect_to prelaunch_thank_you_path, notice: 'apartment saved' }
				end
			else
		      	puts "/////////////////////////////////"
		      	puts @apartment.errors.details
		        format.html { render :new }
	      	end
	    end
	end

	def edit
	  @apartment = Apartment.friendly.find(params[:id])
	end

	def update
		@apartment = Apartment.friendly.find(params[:id])
		@apartment.update_attributes(apartment_params)
  		@apartment.wifi_speed = WifiSpeed.find_by_speed(params[:apartment][:wifi_speed])
  		@apartment.contacts.destroy_all
		number_of_contacts = params[:contact_types].length
		number_of_contacts.times do |x|
		  @apartment.contacts.push(Contact.create(:contact_type => params[:contact_types][x], :value => params[:contact_values][x]))
		end

      	amenities = params[:apartment][:amenities]
      	if amenities == [""]
      	  @apartment.amenities.destroy_all
      	end
		amenities.shift
		all_amenities = Amenity.all.map{|amenity| amenity.id}
		amenities = amenities.map{|amenity| amenity.to_i}
		diff = all_amenities - amenities
		amenities.each do |amenity|
	  	  if !@apartment.amenities.include?(Amenity.find_by_id(amenity))
	  		@apartment.amenities.push(Amenity.find_by_id(amenity))
	  	  end
		end

		if diff != []
		  diff.each do |x|
		  	if @apartment.amenities.include?(x)
		  	  @apartment.amenities.delete(x)
		  	end
		  end
		end
		respond_to do |format|

      	if params[:images]
      		@apartment.pictures.destroy_all
	        first = true
	        params[:images].each { |image|
	          if first
	            @apartment.pictures.push(Picture.create(image: image, apartment: @apartment, front: true))
	            first = false
	          else
	          	@apartment.pictures.push(Picture.create(image: image, apartment: @apartment, front: false))
	          end
	        }
	    elsif params[:direct_uris]
	    	@apartment.pictures.destroy_all
	    	first = true
	        params[:direct_uris].each { |image|
	          if first
	            @apartment.pictures.create(direct_uri: image, apartment: @apartment, front: true)
	            first = false
	          else
	          	@apartment.pictures.create(direct_uri: image, apartment: @apartment, front: false)
	          end
	        }
	    end
      	if @apartment.save
			format.html { redirect_to apartments_path, notice: 'apartment saved' }
		else
	      	puts "/////////////////////////////////"
	      	puts @apartment.errors.details
	        format.html { render :new }
      	end
	end
end
	def show
		@apartment = Apartment.friendly.find(params[:id])
	end

	def search
		# @apartments = Apartment.filter_result(params[:city], params[:amenities], params[:wifi_speed], params[:avg_price], params[:min_stay], params[:bedrooms], params[:apartment_type])
		if params[:city] && params[:city] == "Chiang Mai Thailand"
			params[:city] = "MAYA Lifestyle Shopping Center Chang Phueak Chiang Mai Thailand"
		end
		@apartments = Apartment.where(:approved => true)
		@apartments = @apartments.by_amenities(params[:amenities]) if params[:amenities]
		@apartments = @apartments.within(30, :origin => params[:city]).all if params[:city] != ""
		@apartments = @apartments.by_wifi_speed(params[:wifi_speed].to_i) if params[:wifi_speed]
		@apartments = @apartments.by_bedrooms(params[:bedrooms].to_i) if params[:bedrooms]
		@apartments = @apartments.by_type(params[:apartment_type]) if params[:apartment_type]
		#@apartments = @apartments.by_min_stay(params[:min_stay]) if params[:min_stay] != ""

		if params[:avg_price] && params[:avg_price] != "0"
	      @apartments = @apartments.reject {|a|
	      	a.avg_price.to_i > params[:avg_price].to_i
	      }
	    end

		respond_to do |format|
	      format.js { }
	      format.json {render json: {
	        :response => @apartments
	      }}
		end
	end

	private

	def is_the_owner
		unless current_user && current_user.admin? || current_user == Apartment.friendly.find(params[:id]).user
		  redirect_to apartment_path(Apartment.friendly.find(params[:id]))
		end
	end

	def is_prelaunch
		unless current_user || Time.now > "2017/08/01" || cookies[:has_account]
		  redirect_to root_path
		end
	end

	def send_user_email
	    if current_user
	      from = Email.new(email: "info@belooga.co")
	      to = Email.new(email: current_user.email)
	      subject = "[Belooga][Home Added] - You are awesome!"
	      email_text = "Hey, #{current_user.name} <br>"
	      email_text+= "Thanks for adding one of your favorite homes! We'll review it and publish it in the next 48 hours<br>"
	      email_text+= "Feel free to <a href='http://belooga.co/apartments/new'> keep adding new apartments </a> directly in the platform and help other people finding good places to stay.<br>"
	      email_text+= "Thank you!"
	      content = Content.new(type: "text/html", value: email_text)
	      mail = SendGrid::Mail.new(from, subject, to, content)
	      mail.template_id = '82ff248c-3492-481e-b8dc-6658c78db90d'
	      sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"])
	      response = sg.client.mail._("send").post(request_body: mail.to_json)
	      puts response.status_code
	      puts response.body
	      puts response.headers
	    else
	      from = Email.new(email: cookies[:invitation_email])
	      to = Email.new(email: "info@belooga.co")
	      subject = "[Belooga][Home Added] - New Home Added By Early Adopter"
	      email_text = "Hey,<br>"
	      email_text+= "We have a new beta tester! Please review the apartment and approve it!<br>"
	      email_text+= "Thank you!"
	      content = Content.new(type: "text/html", value: email_text)
	      mail = SendGrid::Mail.new(from, subject, to, content)
	      mail.template_id = '82ff248c-3492-481e-b8dc-6658c78db90d'
	      sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"])
	      response = sg.client.mail._("send").post(request_body: mail.to_json)
	      puts response.status_code
	      puts response.body
	      puts response.headers

	      to = Email.new(email: cookies[:invitation_email])
	      from = Email.new(email: "info@belooga.co")
	      subject = "Thanks for adding a new Home!"
	      email_text = "Hey,<br>"
	      email_text+= "Thanks a lot for adding a new home. We're reviewing it and will send you the invitation over very soon!<br>"
	      email_text+= "Thank you!"
	      content = Content.new(type: "text/html", value: email_text)
	      mail = SendGrid::Mail.new(from, subject, to, content)
	      mail.template_id = '82ff248c-3492-481e-b8dc-6658c78db90d'
	      sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"])
	      response = sg.client.mail._("send").post(request_body: mail.to_json)
	      puts response.status_code
	      puts response.body
	      puts response.headers
	    end
	  end

	def apartment_params
		params.require(:apartment).permit(:name, :address, :apartment_type, :avg_price, :description, :bedrooms)
	end
end
