class Admin::ApartmentsController < AdminController
	def index
		@apartments = Apartment.all
	end

	def new
		@apartment = Apartment.new
	end

	def create
		@apartment = Apartment.create(apartment_params)
		amenities = params[:apartment][:amenities]
		amenities.shift
		amenities.each do |amenity|
			@apartment.amenities.push Amenity.find(amenity)
		end
		@apartment.user = current_user

		respond_to do |format|
	      if @apartment.save
	      	if params[:images]
		        #===== The magic is here ;)
		        count = 0
		        params[:images].each { |image|
		          if count == 0
		            @apartment.pictures.create(image: image, apartment: @apartment, front: true)
		            count +=1
		          else @apartment.pictures.create(image: image, apartment: @apartment, front: false)
		          end
		        }
		    end
	        format.html { redirect_to admin_apartments_path, notice: 'apartment saved' }
	      else
	        format.html { render :new }
	      end
	    end
	end

	def show
		@apartment = Apartment.friendly.find(params[:id])
	end

	private

	def apartment_params
		params.require(:apartment).permit(:name, :street, :city, :country, :amenities, :phone_number, :email, :pictures)
	end
end