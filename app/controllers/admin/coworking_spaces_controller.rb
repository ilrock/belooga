class Admin::CoworkingSpacesController < AdminController
	def index
		@coworking_spaces = CoworkingSpace.all
	end

	def new
		@coworking_space = CoworkingSpace.new
	end

	def create
		@coworking_space = CoworkingSpace.create(coworking_params)

		respond_to do |format|
	      if @coworking_space.save
	        format.html { redirect_to admin_coworking_spaces_path, notice: 'Coworking saved' }
	      else
	        format.html { render :new }
	      end
	    end
	end

	private

	def coworking_params
		params.require(:coworking_space).permit(:name, :street, :city, :country, :website, :phone_number, :email)
	end
end