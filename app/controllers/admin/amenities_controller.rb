class Admin::AmenitiesController < AdminController
	def index
		@amenities = Amenity.all
	end

	def new
		@amenity = Amenity.new
	end

	def create
		@amenity = Amenity.create(amenity_params)

		respond_to do |format|
	      if @amenity.save
	        format.html { redirect_to admin_amenities_path, notice: 'Amenity saved' }
	      else
	        format.html { render :new }
	      end
	    end
	end

	private

	def amenity_params
		params.require(:amenity).permit(:name, :icon)
	end

end