class CitiesController < ApplicationController

	def countries
	end

	def index
		country = CS.countries.key(params[:country])
		cities = CS.states(country).keys.flat_map { |state| CS.cities(state, country) }

		respond_to do |format|
			format.js {}
			format.json {render json: cities}
		end
	end

	def show
		@city = City.friendly.find(params[:id])

		if @city.name != "Chiang Mai"
			@apartments = Apartment.within(10, :origin => @city.name).all
		else
			@apartments = Apartment.within(10, :origin => "MAYA Lifestyle Shopping Center Chang Phueak Chiang Mai Thailand").all
		end
	end
end
