class AdminController < ActionController::Base
  protect_from_forgery with: :exception
  layout 'admin'
  before_filter :is_admin


  def is_admin
  	unless current_user.admin? 
  		redirect_to root_path
  	end
  end
end
