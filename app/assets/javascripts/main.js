function showPage() {
  $('#loader-container').addClass("hidden")
  $('#upload-apt-container').removeClass('invisible');
}

$(document).on('turbolinks:load', function() {
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
	})

	$(document.body).on('click', '.apt-modal-search-btn', function(){
		$('#accommodation-name-modal').modal("hide")
          $('#loader-container').removeClass("hidden")
          $('#upload-apt-container').addClass('invisible');
		  setTimeout(showPage, 3000);
		  $('.upload-apt-section.invisible').removeClass('invisible').hide().fadeIn('3000');
	      $('.btn-next-section').fadeOut('3000');
	});

	$(document.body).on('click', '.add-contact-btn' ,function(){
		$(this).fadeOut('1000');
		$(this).parent().next().children().first().fadeOut('1000');
		$(".contact-row").append($("#new-contact").html());
		$('[data-toggle="tooltip"]').tooltip();
	});

	// $(".next-section-btn").click(function() {
	//     $('html,body').animate({
	//         scrollTop: $(this).parent().parent().next().offset().top},
	//         'slow');
	// });

	$('#user_picture').on('change', function(event) {
	    var files = event.target.files;
	    var image = files[0]
	    // here's the file size
	    console.log(image.size);
	    var reader = new FileReader();

	    reader.onload = function(file) {
	    	console.log(file.target);
	    	$('.user-avatar').attr('src', file.target.result);
	    }
	    reader.readAsDataURL(image);
	    console.log(files);
	});

	$('.filter-option').on('change', function(){
		$.ajax({
		  url: "/apartments/search?"+$('#filter-form').serialize().replace("utf8=%E2%9C%93&","")
		});
	})

	$('.modal-done').on('click', function(){
		if ($('.modal-done').parentsUntil('.row').last().find('input:checked').length > 0){
			var checkedValues = $('.modal-done').parentsUntil('.row').last().find('input:checked').map(function() {
			    return this.value;
			}).get();
			$.ajax({
			  url: "/apartments/search?amenities="+checkedValues,
			});
		}
	})

	var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                	var html = "<div class='col-md-4'>"
                		html+= "<div class='apt-thumb-container'>"
                		html+= "<img class='image-thumbnail' src="+ event.target.result +" style='height:100px; width:100px'>"
                		html+= "</div>"
                		html+= "</div>"
                	$(placeToInsertImagePreview).append(html);
                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#images_').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });

	$("a[href='#contact-details']").on('shown.bs.tab', function (e) {
	  //mixpanel.track("Details checked");
	})
	
	//$.scrollSpeed(100, 800);

	$("#avg_price").on('input',function() { 
	  input_value = $(this).val();
	  $('.budget-value').html(input_value);
	});

	$('.home-apt-card').css('height', $('.home-apt-card').css('width'));

	$(window).on('resize', function(){
		$('.home-apt-card').css('height', $('.home-apt-card').css('width'));
	})

	$('.form-control').on('focus', function(){
		$(this).addClass('shadow');
	})
	$('.form-control').on('blur', function(){
		$(this).removeClass('shadow');
	})

	

	$(".list-map-toggle").bootstrapSwitch();
	
	$(".list-map-toggle").on('switchChange.bootstrapSwitch', function(event, state) {
	  	$('.list-container').toggleClass("hidden");
	  	$('.map-container').toggleClass("hidden");
	  	google.maps.event.trigger(theMap, 'resize');
	});

	$('.looking-button').on('click', function(){
		$('.modal').css('display', 'block');
	});

	active = $('.wizard-container .active')
 	$('body').on('click','.wizard-container .active .wizard-btn',function(){
 		if (validateFormControl()){
			active.removeClass('active').addClass('invisible absolute');
			active = active.next();
			active.removeClass('invisible absolute').addClass('active');
			$('.progress .progress-bar').css('width', active.attr('data-progress'));
			$('.progress .progress-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
	    		$('.wizard-icon[data-progress='+ active.attr('data-progress').replace('%','') +']').addClass('activated');
	  		});
	  	}
	});
 	$('.remove-blank select.form-control').find("option:first-child").remove();
	$('select.form-control').find("option:first-child").attr('disabled', true);

	if ($('.made-easy-container').length > 0){
		$(window).on('scroll', function(){
			var bottom_of_object = $('.made-easy-container').position().top + $('.made-easy-container').outerHeight();
	        var bottom_of_window = $(window).scrollTop() + $(window).height();
	            
	            /* If the object is completely visible in the window, fade it it */
	        if( bottom_of_window > bottom_of_object + 150){
			//if ($(window).scrollTop() > $('.made-easy-container').offset().top){
				console
				$('.card.invisible').removeClass('invisible').hide().fadeIn('3000');
			}
		});
	}

	// $('.say-thanks').on('click', function(){
	// 	$.ajax({
	// 	  dataType: "json",
	// 	  method: "post",
	// 	  url: document.location.pathname + '/thank',
	// 	  success: function(data){
	// 	  	console.log(data);
	// 	  	if (data.response.status == "Success"){
	// 	  		$('.uploaded-by-container').attr('data-content', data.response.thank_yous);
	// 	  	}
	// 	  	else{
	// 	  		alert("You have already thanked the user for this apartment");
	// 	  	}
	// 	  }
	// 	});
	// });

	function initMap(id) {
	  var map = new google.maps.Map(document.getElementById('map'), {
	    center: {lat: -33.866, lng: 151.196},
	    zoom: 15
	  });

	  var infowindow = new google.maps.InfoWindow();
	  var service = new google.maps.places.PlacesService(map);

	  service.getDetails({
	    placeId: id
	  }, function(place, status) {
	    if (status === "OK") {
	      geocoder = new google.maps.Geocoder();
	      let latlng = {lat: parseFloat(place.geometry.location.lat()), lng: parseFloat(place.geometry.location.lng())};
	      geocoder.geocode({'location': latlng}, function(results, status) {
          	if (status === 'OK') {
          		console.log(results);
          		for (var i=0; i<results.length; i++){
          			for (var j=0; j<results[i].address_components.length; j++){
          				if (results[i].address_components[j].types[0] == "locality"){
          					$('#apartment_city').val(results[i].address_components[j].long_name);
          				}
          			}          			
          		}
          	}
          });
          $('#accommodation-name-modal').modal("hide")
          $('#loader-container').removeClass("hidden")
          $('#upload-apt-container').addClass('invisible');
		  setTimeout(showPage, 3000);
	      $('.upload-apt-section.invisible').removeClass('invisible').hide().fadeIn('3000');
	      $('.btn-next-section').fadeOut('3000');
	      // -- Google Bug 63317126
	      if (place.photos){
	      	var html = "<div class='col-md-12 text-center'> <div class='alert alert-success' role='alert'><strong>Woohoop!</strong> We've done the heavy lifting for you and found already some pictures :) </div></div>"
	      	for (var i=0; i<place.photos.length; i++){
		      		html+= "<div class='col-md-4'>"
		      		html+= "<div class='apt-thumb-container text-center'>"
		      	//	html+= "<div class='delete-img-btn' style='position:absolute; width:20px; height:20px; padding-left:5px; background: red;' data-counter=picture-no-"+i+">x</div>"
	                html+= "<img class='image-thumbnail' src="+ place.photos[i].getUrl(({maxWidth: 1260})) +" style='height:100px; width:100px; padding-top: 15px;'>"
	                html+= "</div>"
	                html+= "</div>"
	            $('<input>').attr('type','hidden').attr('name', 'direct_uris[]').attr('data-counter', "picture-no-"+i).val(place.photos[i].getUrl(({maxWidth: 1260}))).appendTo('form');
		    }
		    $("div.gallery").append(html);
	     	$('#images_').attr('disabled', true);
	      }

	       $('#apartment_name').val(place.name);
	      // $('#apartment_street').val(place.vicinity.split(",")[0]);
	      // $('#apartment_country').val(place.formatted_address.split(",")[place.formatted_address.split(",").length - 1]);
	      // $('#city').val(place.vicinity.split(",")[1]);
	      $('#apartment_address').val(place.formatted_address);
	      $('#apartment_link').val(place.website);
	    } else console.log("error");
	  });
	}

	$(document.body).on('click', '.delete-img-btn' ,function(){
		console.log($(this).attr('data-counter'));
		$(this).parent().parent().remove()
		$("[data-counter='"+ $(this).attr('data-counter') +"']").remove()
	});

	var defaultBounds = new google.maps.LatLngBounds(
	    new google.maps.LatLng(-90,-180),
	    new google.maps.LatLng(90,180)
	);

	var input = document.getElementById("apartment_name");
	var options = {
		bounds: defaultBounds
	};

	autocomplete = new google.maps.places.Autocomplete(input, options);

	autocomplete.addListener('place_changed', function() {
		initMap(autocomplete.getPlace().place_id);
	    //console.log(autocomplete.getPlace());
	});

	var inputStreet = document.getElementById("apartment_address");
	autocompleteStreet = new google.maps.places.Autocomplete(inputStreet, options);

	var selectCity = document.getElementsByClassName("select-city")[0];
	var hpAutocomplete = new google.maps.places.Autocomplete(selectCity, options);
	hpAutocomplete.addListener('place_changed', function() {
	    console.log(hpAutocomplete.getPlace());
	    let lat = hpAutocomplete.getPlace().geometry.location.lat()
	    let lng = hpAutocomplete.getPlace().geometry.location.lng()
	    $('.modal-city-btn').attr('href', apartmentsUri + "?lat=" + lat + "&lng="+ lng+ "&city="+ $('.select-city').val());
	    $('.select-city').blur();
	});
	var apartmentsUri = $('.modal-city-btn').attr('href');

	geocoder = new google.maps.Geocoder();
	var workfromPlaces;

	// Get places from workfrom
	function getWorkfromPlaces() {
	    //In this case it gets the address from an element on the page, but obviously you  could just pass it to the method instead
	    var address = $('.address').text();
	    geocoder.geocode( { 'address' : address }, function(results, status) {
	        if( status == google.maps.GeocoderStatus.OK ) {
			    $.ajax({
			        url: "//api.workfrom.co/places/ll/"+results[0].geometry.location.lat()+","+results[0].geometry.location.lng()+"?appid=hbtcBawThQbjJzmj",
			        dataType: 'json',
			        success: function(data){
			        	if (data.response.length > 5){
			        		workfromPlaces = data.response.filter(function(x){
			        			return x.download >= 20;
			        		});
			        	} else  workfromPlaces = data.response;

			        	console.log(workfromPlaces);
			        }
			    }).done(function(){
			    	var html = "";
			    	if (workfromPlaces.length == 0){
			    			html+= "<div class='col-12 venue'>";
					    	html+= "No places to work from found nearby"
		                  	html+= "</div>"
			    	} else{
			    		for (var i=0; i<workfromPlaces.length; i++){
					    	html+= "<div class='col-12 venue'>";
					    	html+= "<a target='_blank' href=https://workfrom.co/'"+ workfromPlaces[i].slug +"'><span> <img class='rounded-circle' src='" + assignBadge(workfromPlaces[i].download) + "'><span>" + workfromPlaces[i].title +" - "+ parseInt(workfromPlaces[i].distance * 1609) +"m </a>"
		                  	html+= "</div>"
				    	}
			    	}
			    	$('.workfrom .row').append(html);
			    });
	        } else {
	            $('.workfrom').addClass('hidden');
	        }
	    });
	}

	function assignBadge(speed){
		speed = parseFloat(speed);
		if (speed < 5){
			return "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-5.png"
		} else if (speed < 10){
			return "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-10.png"
		} else if (speed < 20){
			return "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-20.png"
		} else if (speed < 50){
			return "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-50.png"
		} else	return "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-50%2B.png" 
	}

	getWorkfromPlaces();

	function validateFormControl() {
		let inputs = $('.wizard-container .active .form-control')
		let feedbackDiv = '<div class="form-control-feedback">Can\'t be blank!</div>'
		let count = 0;
		for (var i=0; i<inputs.length; i++){
			if ($(inputs[i]).val() == ""){
				$(inputs[i]).parent().addClass('has-danger').find('.wizard-feedback').html(feedbackDiv);
				count++
			}
			else{
				$(inputs[i]).parent().removeClass('has-danger').find('.wizard-feedback').html("");
			}
		}
		if (count > 0){
			console.log('false');
			return false		
		}
		else {
			console.log('true')
			return true
		}
	}
});
