class CreateBenefits < ActiveRecord::Migration[5.0]
  def change
    create_table :associations do |t|
      t.references :apartment, foreign_key: true
      t.references :amenity, foreign_key: true
      t.timestamps
    end
  end
end
