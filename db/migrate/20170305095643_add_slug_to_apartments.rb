class AddSlugToApartments < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :slug, :string, :uniq => true
  end
end
