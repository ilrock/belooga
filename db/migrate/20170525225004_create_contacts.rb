class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
    	t.string :contact_type
    	t.string :value
    	t.string :icon
    	t.references :apartment
    end
  end
end
