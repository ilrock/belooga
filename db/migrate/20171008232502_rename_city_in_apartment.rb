class RenameCityInApartment < ActiveRecord::Migration[5.0]
  def change
    rename_column :apartments, :city, :city_old
  end
end
