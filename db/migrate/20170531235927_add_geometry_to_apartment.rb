class AddGeometryToApartment < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :lat, :string
  	add_column :apartments, :lng, :string
  end
end
