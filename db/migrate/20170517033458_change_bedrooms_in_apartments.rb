class ChangeBedroomsInApartments < ActiveRecord::Migration[5.0]
  def change
  	remove_column :apartments, :bedrooms, :string
  	add_column :apartments, :bedrooms, :integer
  end
end
