class AddMessageToWifiSpeeds < ActiveRecord::Migration[5.0]
  def change
  	add_column :wifi_speeds, :message, :string
  end
end
