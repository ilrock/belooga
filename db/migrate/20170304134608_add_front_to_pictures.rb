class AddFrontToPictures < ActiveRecord::Migration[5.0]
  def change
  	add_column :pictures, :front, :boolean, :null => false, :default => false
  end
end
