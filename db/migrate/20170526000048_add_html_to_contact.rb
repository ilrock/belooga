class AddHtmlToContact < ActiveRecord::Migration[5.0]
  def change
  	add_column :contacts, :html, :string
  end
end
