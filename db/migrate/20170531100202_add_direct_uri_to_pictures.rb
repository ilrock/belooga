class AddDirectUriToPictures < ActiveRecord::Migration[5.0]
  def change
  	add_column :pictures, :direct_uri, :string
  end
end
