class AddApartmentTypeToApartment < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :apartment_type, :string
  	add_column :apartments, :bedrooms, :string
  end
end
