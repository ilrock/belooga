class ChangeGeometryInApartment < ActiveRecord::Migration[5.0]
  def change
  	remove_column :apartments, :lat, :string
  	remove_column :apartments, :lng, :string

  	add_column :apartments, :lat, :float
  	add_column :apartments, :lng, :float
  end
end
