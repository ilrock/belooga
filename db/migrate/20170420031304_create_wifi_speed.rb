class CreateWifiSpeed < ActiveRecord::Migration[5.0]
  def change
    create_table :wifi_speeds do |t|
    	t.string :integer
    	t.string :badge
    	t.integer :speed
    end
  end
end
