class CreateFeedbacksTable < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|
    	t.string :name
    	t.string :email
    	t.text :body
    	t.string :feedback_type

    	t.timestamps
    end
  end
end
