class AddApprovalToApartments < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :approved, :boolean, :default => false, nil => false
  end
end
