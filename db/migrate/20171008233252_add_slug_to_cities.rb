class AddSlugToCities < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :slug, :string, :uniq => true
  end
end
