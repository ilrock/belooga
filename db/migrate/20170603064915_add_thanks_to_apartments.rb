class AddThanksToApartments < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :thank_yous, :integer, :default => 0, :nil => 0
  end
end
