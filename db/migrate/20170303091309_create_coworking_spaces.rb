class CreateCoworkingSpaces < ActiveRecord::Migration[5.0]
  def change
    create_table :coworking_spaces do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :country
      t.string :website
      t.string :phone_number
      t.string :email
      t.timestamps
    end
  end
end
