class AddPictureToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :picture_id, :integer
  	add_column :pictures, :user_id, :integer
  end
end
