class AddLinkToApartment < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :link, :string
  end
end
