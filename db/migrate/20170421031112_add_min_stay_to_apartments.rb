class AddMinStayToApartments < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :min_stay, :string
  end
end
