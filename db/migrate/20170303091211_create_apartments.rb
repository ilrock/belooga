class CreateApartments < ActiveRecord::Migration[5.0]
  def change
    create_table :apartments do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :country
      t.string :phone_number
      t.string :email
      t.timestamps
    end
  end
end
