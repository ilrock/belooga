class AddCityToApartments < ActiveRecord::Migration[5.0]
  def change
  	remove_column :apartments, :city
  	add_column :apartments, :city_id, :integer
  end
end
