class AddHomeOwnerToApartments < ActiveRecord::Migration[5.0]
  def change
  	add_column :apartments, :home_owner, :boolean, :default => false, :nil => false
  end
end
