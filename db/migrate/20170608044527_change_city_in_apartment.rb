class ChangeCityInApartment < ActiveRecord::Migration[5.0]
  def change
  	remove_column :apartments, :city_id, :integer
  	add_column :apartments, :city, :string
  end
end
