# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171008234513) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "amenities", force: :cascade do |t|
    t.string   "name"
    t.string   "icon"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "apartment_id"
  end

  create_table "apartments", force: :cascade do |t|
    t.string   "name"
    t.string   "street"
    t.string   "country"
    t.string   "phone_number"
    t.string   "email"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "amenity_id"
    t.integer  "user_id"
    t.string   "slug"
    t.integer  "wifi_speed_id"
    t.string   "apartment_type"
    t.string   "avg_price"
    t.string   "min_stay"
    t.text     "description"
    t.integer  "bedrooms"
    t.string   "link"
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.integer  "thank_yous",     default: 0
    t.string   "city_old"
    t.boolean  "approved",       default: false
    t.boolean  "home_owner",     default: false
    t.integer  "city_id"
  end

  create_table "associations", force: :cascade do |t|
    t.integer  "apartment_id"
    t.integer  "amenity_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["amenity_id"], name: "index_associations_on_amenity_id", using: :btree
    t.index ["apartment_id"], name: "index_associations_on_apartment_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.string   "slug"
    t.text     "description"
  end

  create_table "contacts", force: :cascade do |t|
    t.string  "contact_type"
    t.string  "value"
    t.string  "icon"
    t.integer "apartment_id"
    t.string  "html"
    t.index ["apartment_id"], name: "index_contacts_on_apartment_id", using: :btree
  end

  create_table "coworking_spaces", force: :cascade do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.string   "country"
    t.string   "website"
    t.string   "phone_number"
    t.string   "email"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.text     "body"
    t.string   "feedback_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "pictures", force: :cascade do |t|
    t.string   "description"
    t.string   "image"
    t.integer  "apartment_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "front",              default: false, null: false
    t.integer  "user_id"
    t.string   "direct_uri"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false, null: false
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.integer  "picture_id"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "wifi_speeds", force: :cascade do |t|
    t.string  "integer"
    t.string  "badge"
    t.integer "speed"
    t.string  "message"
  end

  add_foreign_key "associations", "amenities"
  add_foreign_key "associations", "apartments"
end
