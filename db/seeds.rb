# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
amenity_list = [
	["kitchen", "fa fa-cutlery"],
	["air conditioning", "fa fa-snowflake-o"],
	["weelchair access", "fa fa-wheelchair"],
	["television", "fa fa-television"],
	["breakfast", "fa fa-coffee"],
	["bath tub", "fa fa-bath"],
	["shower", "fa fa-shower"],
	["heating", "fa fa-thermometer-half"]
]

amenity_list.each do |name, icon|
  Amenity.create( name: name, icon: icon )
end

User.create(:email => "admin@admin.com", :name => "admin", :admin => true, :password => "password", :password_confirmation => "password")

wifi_list = [
	[5, "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-5.png", "At least 5 Mbps"],
	[10, "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-10.png", "At least 10 Mbps"],
	[20, "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-20.png", "At least 20 Mbps"],
	[50, "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-50.png", "At least 50 Mbps"],
	[51, "//s3.eu-west-2.amazonaws.com/belooga/Images/wifi-50+.png", "More than 50 Mbps"]
]

wifi_list.each do |speed, badge, message|
  WifiSpeed.create( speed: speed, badge: badge, message: message )
end

apartments = ["Apartment 1", "Apartment 2", "Apartment 3", "Apartment 4", "Apartment 5", "Apartment 6"]

apartments.each do |apartment|
	Apartment.create(
		name: apartment,
		user_id: User.first.id,
		wifi_speed_id: WifiSpeed.last.id,
		apartment_type: "House",
		avg_price: 2000,
		description: apartment,
		bedrooms: 2,
		address: "Via G. Marafioti 9B, Catanzaro, 88100",
		pictures: [
			Picture.create(direct_uri: "http://media.npr.org/assets/img/2015/09/23/ap_836720500193-13f1674f764e5180cf9f3349cfef258d181f2b32-s900-c85.jpg")
		],
		contacts: [
			Contact.create(
				contact_type: "Phone Number",
				value: "123456789"
			)
		],
		approved: true
	)
end
