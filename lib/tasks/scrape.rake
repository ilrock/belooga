require 'open-uri'
namespace :scrape do
  desc "TODO"
  task yournewbase: :environment do

    doc = Nokogiri::HTML(open("https://www.yournewbase.com/sitemap.xml"))

    urls = doc.xpath("//loc").map { |node| node.content }
    
    urls = urls.select do |url|
      url.include? "properties"
    end

    browser = Watir::Browser.new :chrome, switches: ['--incognito']

    urls.each do |url|
      browser.goto url

      sleep(5)

      browser.a(class: "dropdown-toggle").click
      browser.ul(id: "currencySelector").lis.first.a.click

      apartment = Apartment.new

      apartment.name = browser.div(class: "propertynamecolumn").h1.text
      puts "Setting name..."

      apartment.address = browser.div(class: "propertyaddresscolumn").ps.first.text
      puts "Setting address..."
      
      apartment.contacts.push Contact.create(contact_type: "Phone Number", value: browser.div(class: "propertyaddresscolumn").ps.last.text)
      puts "Setting phone number..."

      apartment.contacts.push Contact.create(contact_type: "Email", value: browser.divs(class: "websiteemaildiv").last.p.a.href)
      puts "Setting email"

      apartment.contacts.push Contact.create(contact_type: "Website", value: browser.divs(class: "websiteemaildiv").first.p.a.href)
      puts "Setting website"

      if browser.div(class: "propertyinfocolumn", index: 1).b.text.include? "$"
        apartment.avg_price = browser.div(class: "propertyinfocolumn", index: 1).b.text.split(" ").first.gsub("$", "")
      else
        apartment.avg_price = browser.div(class: "roomtyperate").p.b.text.split("/").first.gsub("$", "")
      end

      puts "Setting price..."

      if browser.divs(class: "roomtypeinfo")
        apartment.bedrooms = browser.div(class: "roomtypeinfo").div.p(index: 2).bAp
      else
        apartment.bedrooms = 1
      end

      puts "Setting bedrooms..."

      if browser.div(class: "carousel-inner").exists?
        images = browser.div(class: "carousel-inner").divs(class: "item").map{ |slide| slide.img.src }.each do |image|
          apartment.pictures.push(Picture.create(direct_uri: image))
        end
        puts "Setting images"
      end

      apartment.wifi_speed_id = 2

      apartment.user_id = User.first.id

      if apartment.save
        puts "########### Apartment created ###########"
      else
        puts "!!!!!!!!!!! Errors !!!!!!!!!!!!!"
        sleep(10000)
      end
    end
  end

end
